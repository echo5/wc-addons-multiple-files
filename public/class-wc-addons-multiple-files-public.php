<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://echo5digital.com
 * @since      1.0.0
 *
 * @package    Wc_Addons_Multiple_Files
 * @subpackage Wc_Addons_Multiple_Files/public
 */

class Wc_Addons_Multiple_Files_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Get WC Addon templates
	 *
	 * @param string $located
	 * @param string $template_name
	 * @param array $args
	 * @param string $template_path
	 * @param string $default_path
	 * @return string
	 */
	public function get_addon_templates( $located, $template_name, $args, $template_path, $default_path ) {    
		if ( 'addons/file_upload.php' == $template_name ) {
			$located = plugin_dir_path( __FILE__ ) . 'partials/file_upload.php';
		}
		return $located;
	}

	/**
	 * Add cart item data.
	 *
	 * @param array $cart_item_meta Cart item meta data.
	 * @param int   $product_id     Product ID.
	 * @param bool  $test           If this is a test i.e. just getting data but not adding to cart. Used to prevent uploads.
	 *
	 * @throws Exception
	 *
	 * @return array
	 */
	public function add_cart_item_data( $cart_item_meta, $product_id, $post_data = null, $test = false ) {
		if ( is_null( $post_data ) && isset( $_POST ) ) {
			$post_data = $_POST;
		}

		// Technically we could just use $post_data['add-to-cart'] for product id
		// however since we don't know if $product_id has been filtered higher up the chain
		// and to be on safe side, use this check.
		if ( ! empty( $post_data['add-to-cart'] ) ) {
			$product_id = $post_data['add-to-cart'];
		}

		$product_addons = get_product_addons( $product_id );

		if ( empty( $cart_item_meta['addons'] ) ) {
			$cart_item_meta['addons'] = array();
		}

		if ( is_array( $product_addons ) && ! empty( $product_addons ) ) {
			include_once( dirname( __FILE__ ) . '/fields/file_uploads.php' );

			foreach ( $product_addons as $addon ) {

				$value = isset( $post_data[ 'addon-' . $addon['field-name'] ] ) ? $post_data[ 'addon-' . $addon['field-name'] ] : '';

				if ( is_array( $value ) ) {
					$value = array_map( 'stripslashes', $value );
				} else {
					$value = stripslashes( $value );
				}

				if ( $addon['type'] == 'file_upload' ) {
					$field = new Product_Addon_Field_File_Uploads( $addon, $value, $test );
					$data = $field->get_cart_item_data( $addon );
					if ( is_wp_error( $data ) ) {
						// Throw exception for add_to_cart to pickup.
						throw new Exception( $data->get_error_message() );
					} elseif ( $data ) {
						$cart_item_meta['addons'] = array_merge( $cart_item_meta['addons'], apply_filters( 'woocommerce_product_addon_cart_item_data', $data, $addon, $product_id, $post_data ) );
					}
				}

			}
		}

		return $cart_item_meta;
	}

	/**
	 * Fix the display of uploaded files.
	 *
	 * @param  string $meta_value Meta value.
	 * @return string
	 */
	public function fix_file_uploaded_display( $meta_value, $meta, $item ) {
		global $wp;

		// If the value is a string, is a URL to an uploaded file, and we're not in the WC API, reformat this string as an anchor tag.
		if ( is_string( $meta_value ) && ! isset( $wp->query_vars['wc-api'] ) && false !== strpos( $meta_value, '/product_addons_uploads/' ) ) {
			$uploads = explode( ',', $meta_value );
			$meta_value = '';
			foreach ( $uploads as $upload ) {
				$name = basename( $upload );
				$meta_value .= '<a href="' . esc_url( $upload ) . '">' . esc_html( $name ) . '</a>';
			}
		}

		// Before fixing newlines issue for textarea, ensure we're dealing with textarea type
		$product_fields = is_callable( array( $item, 'get_product_id' ) ) ?
			get_post_meta( $item->get_product_id(), '_product_addons', true ) :
			array();

		if ( ! empty( $product_fields ) && is_callable( array( $meta, 'get_data' ) ) ) {
			$meta_data      = $meta->get_data();
			$product_fields = array_filter( $product_fields, function( $field ) use ( $meta_data ) {
				return isset( $field['name'] ) && isset( $field['type'] )
					&& $field['name'] == $meta_data['key'] && 'custom_textarea' == $field['type'];
			} );

			if ( ! empty( $product_fields ) ) {
				// Overwrite display value since core has already removed newlines
				$meta_value = $meta->value;
			}
		}

		return $meta_value;
	}

	/**
	 * Remove default meta value filters for file uploads
	 *
	 * @return void
	 */
	public function remove_wc_addons_meta_value_filter() {
		remove_filter( 'woocommerce_order_item_display_meta_value', array( $GLOBALS['Product_Addon_Display'], 'fix_file_uploaded_display' ), 10 );
	}

}
