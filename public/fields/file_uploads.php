<?php
/**
 * File Upload field
 */
class Product_Addon_Field_File_Uploads extends Product_Addon_Field_File_Upload {

	/**
	 * Process this field after being posted
	 * @return array on success, WP_ERROR on failure
	 */
	public function get_cart_item_data() {
		$cart_item_data           = array();

		foreach ( $this->addon['options'] as $option ) {
			$field_name = $this->get_field_name() . '-' . sanitize_title( $option['label'] ) . '-multiple';
			$this_data  = array(
				'name'    => $this->get_option_label( $option ),
				'price'   => $this->get_option_price( $option ),
				'value'   => '',
				'display' => ''
			);

			if ( ! empty( $_FILES[ $field_name ] ) && ! empty( $_FILES[ $field_name ]['name'][0] ) && ! $this->test ) {
                $uploads = array();
                $count = count( $_FILES[ $field_name ]['name'] );
                for( $i = 0; $i < $count; $i++ ) {
                    $upload = $this->handle_upload( array(
                        'name' => $_FILES[ $field_name ]['name'][$i],
                        'type' => $_FILES[ $field_name ]['type'][$i],
                        'tmp_name' => $_FILES[ $field_name ]['tmp_name'][$i],
                        'size' => $_FILES[ $field_name ]['size'][$i]
                    ) );

                    if ( empty( $upload['error'] ) && ! empty( $upload['file'] ) ) {
                        $uploads[] = $upload['url'];
                    } else {
                        return new WP_Error( 'addon-error', $upload['error'] );
                    }
                }
                $value                = wc_clean( implode( ',', $uploads ) );
                $this_data['value']   = wc_clean( implode( ',', $uploads ) );
                $this_data['display'] = implode( ',', array_map( function($v) { return basename( wc_clean( $v ) ); }, $uploads ) );
                $cart_item_data[] = $this_data;
			} elseif ( isset( $this->value[ sanitize_title( $option['label'] ) ] ) ) {
				$this_data['value']   = $this->value[ sanitize_title( $option['label'] ) ];
				$this_data['display'] = basename( $this->value[ sanitize_title( $option['label'] ) ] );
				$cart_item_data[] = $this_data;
			}
		}

		return $cart_item_data;
	}

}
