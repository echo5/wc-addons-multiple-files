<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://echo5digital.com
 * @since      1.0.0
 *
 * @package    Wc_Addons_Multiple_Files
 * @subpackage Wc_Addons_Multiple_Files/includes
 */

class Wc_Addons_Multiple_Files_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'wc-addons-multiple-files',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
