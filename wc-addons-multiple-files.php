<?php

/**
 * The plugin bootstrap file
 *
 * @link              https://echo5digital.com
 * @since             1.0.0
 * @package           Wc_Addons_Multiple_Files
 *
 * @wordpress-plugin
 * Plugin Name:       WC Addons Multiple Files
 * Plugin URI:        #
 * Description:       This plugin allows the WooCommerce Product Add-on File upload field to accept multiple files
 * Version:           1.0.0
 * Author:            Joshua Flowers
 * Author URI:        https://echo5digital.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wc-addons-multiple-files
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WC_ADDONS_MULTIPLE_FILES', '1.0.0' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wc-addons-multiple-files.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wc_addons_multiple_files() {

	$plugin = new Wc_Addons_Multiple_Files();
	$plugin->run();

}
run_wc_addons_multiple_files();
